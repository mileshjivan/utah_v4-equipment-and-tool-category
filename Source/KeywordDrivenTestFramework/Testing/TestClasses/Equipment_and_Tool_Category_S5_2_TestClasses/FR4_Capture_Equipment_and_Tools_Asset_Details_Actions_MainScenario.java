/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author smabe
 */
@KeywordAnnotation(
        Keyword = "FR4-Capture Equipment and Tools Asset Details Actions v5.2 - Main Scenario",
        createNewBrowserInstance = false
)
public class FR4_Capture_Equipment_and_Tools_Asset_Details_Actions_MainScenario extends BaseClass {

    String error = "";

    public FR4_Capture_Equipment_and_Tools_Asset_Details_Actions_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!Actions()) {
            return narrator.testFailed("Actions Failed due - " + error);
        }
        return narrator.finalizeTest("Successfully Capture Asset Schedule");
    }

    public boolean Actions() {

        //Actions        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Actions_Tab())) {
            error = "Failed to wait for Actions Tab";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Actions_Tab())) {
            error = "Failed to click on Actions Tab";
            return false;
        }
        
        //Add button
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Actions_AddButton())) {
            error = "Failed to wait for Add button";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Actions_AddButton())) {
            error = "Failed to click on Add button";
            return false;
        }

         pause(8000);
        //Processflow
         if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.actionsProcessFlow()))
        {
            error = "Failed to locate Processflow button";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.actionsProcessFlow()))
        {
            error = "Failed to click on Processflow button";
            return false;
        }
        
        //Action description
           if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.actionDescription())) 
        {
            error = "Failed to locate Action description field";
            return false;
        }
        
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.actionDescription(),testData.getData("Action description"))) 
        {
            error = "Failed to enter text in Action description field";
            return false;
        }
               
        //Department Responsible
         if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.deptResponsibleDropdown()))
        {
            error = "Failed to locate Department Responsible";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.deptResponsibleDropdown()))
        {
            error = "Failed to click on Department Responsible";
            return false;
        }
         
       if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.singleSelect(testData.getData("Department Responsible"))))
        {
            error = "Failed to locate Department Responsible value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.singleSelect(testData.getData("Department Responsible"))))
        {
            error = "Failed to click on Department Responsible Dropdown value";
            return false;
        } 
        
        //Responsible person
         if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.responsiblePersonDropdown()))
        {
            error = "Failed to locate Responsible person";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.responsiblePersonDropdown()))
        {
            error = "Failed to click on Responsible person";
            return false;
        }
         
       if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.responsiblePersonDropdownValue(testData.getData("Responsible person"))))
        {
            error = "Failed to locate Responsible person value";
            return false;
        }
        
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.responsiblePersonDropdownValue(testData.getData("Responsible person"))))
        {
            error = "Failed to click on Responsible person Dropdown value";
            return false;
        } 
        
        //Action due date
        String actionduedate=SeleniumDriverInstance.getADate("", 0);
         
        if (!SeleniumDriverInstance.enterTextByXpath(Equipment_and_Tool_Category_PageObjects.actionDueDateXpath(), actionduedate))
        {
            error = "Failed to enter Action due Date";
            return false;
        }
        
        //Save button        
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.Save_Button_Actions())) {
            error = "Failed to wait for 'Save' button.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.Save_Button_Actions())) {
            error = "Failed to click on 'Save' button.";
            return false;
        }

        pause(5000);
        //Saving mask
        if (!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.saveWait2())) {
            error = "Webside too long to load wait reached the time out";
            return false;
        }

        //Validate if the record has been saved or not.
        if (!SeleniumDriverInstance.waitForElementsByXpath(Equipment_and_Tool_Category_PageObjects.validateSave())) {
            error = "Failed to wait for Save validation.";
            return false;
        }
        String SaveFloat = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.validateSave());

        if (!SaveFloat.equals("Record saved")) {
            narrator.stepPassedWithScreenShot("Failed to save record.");
            return false;
        }
        narrator.stepPassedWithScreenShot(SaveFloat + ": successfully.");
        String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.actionsRecordNumber_xpath()).split("#");
        setRecordId(retrieveMessage[1]);
        narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
           
        pause(5000);
        return true;
    }

}
