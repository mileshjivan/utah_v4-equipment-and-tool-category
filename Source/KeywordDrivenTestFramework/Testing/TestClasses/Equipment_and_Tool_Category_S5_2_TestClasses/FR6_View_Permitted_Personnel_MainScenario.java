/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package KeywordDrivenTestFramework.Testing.TestClasses.Equipment_and_Tool_Category_S5_2_TestClasses;

import KeywordDrivenTestFramework.Core.BaseClass;
import static KeywordDrivenTestFramework.Core.BaseClass.SeleniumDriverInstance;
import static KeywordDrivenTestFramework.Core.BaseClass.endDate;
import static KeywordDrivenTestFramework.Core.BaseClass.narrator;
import static KeywordDrivenTestFramework.Core.BaseClass.startDate;
import KeywordDrivenTestFramework.Entities.KeywordAnnotation;
import KeywordDrivenTestFramework.Entities.TestResult;
import KeywordDrivenTestFramework.Testing.PageObjects.Equipment_and_Tool_Category_V5_2_PageObjects.Equipment_and_Tool_Category_PageObjects;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author SMabe
 */
@KeywordAnnotation(
        Keyword = "FR6-View Permitted Personnel v5.2 - Main Scenario",
        createNewBrowserInstance = false
)

public class FR6_View_Permitted_Personnel_MainScenario extends BaseClass {
    String error = "";

    public FR6_View_Permitted_Personnel_MainScenario() {
        Calendar cal = Calendar.getInstance();
        startDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
        cal.add(Calendar.DAY_OF_MONTH, 3);
        endDate = new SimpleDateFormat("YYYY-MM-dd").format(cal.getTime());
    }

    public TestResult executeTest() {
        if (!View_Permitted_Personnel()) {
            return narrator.testFailed("Asset Details Failed due - " + error);
        }

        return narrator.finalizeTest("Successfully Viewed Permitted Personnel.");
    }

    public boolean View_Permitted_Personnel() {
//        //Asset Details
//        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
//            if(!SeleniumDriverInstance.waitForElementPresentByXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
//                error = "Failed to wait for the Asset Details.";
//                return false;
//            }
//        }
//        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.AssestDetails())){
//            error = "Failed to wait for the Asset Details.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Asset details record ");
//        
//        //Process flow
//        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment())) {
//            error = "Failed to wait for 'Process flow' button.";
//            return false;
//        }
//        pause(2000);
//        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.process_flow_Equipment())) {
//            error = "Failed to click on 'Process flow' button.";
//            return false;
//        }
//        narrator.stepPassedWithScreenShot("Successfully clicked the process flow.");
        
        //Navigate to Required Permits
        if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.RequiredPermits_Tab())) {
            error = "Failed to wait for Required Permits tab.";
            return false;
        }
        if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.RequiredPermits_Tab())) {
            error = "Failed to click Required Permits tab.";
            return false;
        }
        narrator.stepPassedWithScreenShot("Successfully navigated to Required Permits tab.");
        
        //Permitted Personnel
        pause(5000);
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.PermittedPersonnel_Panel())){
            error = "Failed to wait for the permmitted personnel panel.";
            return false;
        }
        
        //Permitted Personnel record
        if(!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.PermittedPersonnel_Record(getData("Impact type")))){
            error = "Failed to wait for the permmitted personnel record.";
            return false;
        }
        if(!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.PermittedPersonnel_Record(getData("Impact type")))){
            error = "Failed to click the permmitted personnel record.";
            return false;
        }
        
         //Personal Operating Permits processflow
           pause(5000);
           if (!SeleniumDriverInstance.waitForElementByXpath(Equipment_and_Tool_Category_PageObjects.POPProcessflow()))
            {
                error = "Failed to wait for Personal Operating Permits processflow";
                return false;
            }
            if (!SeleniumDriverInstance.clickElementbyXpath(Equipment_and_Tool_Category_PageObjects.POPProcessflow()))
            {
                error = "Failed to click on Personal Operating Permits processflow";
                return false;
            }
            
           narrator.stepPassedWithScreenShot("Processflow in edit phase");
           String[] retrieveMessage = SeleniumDriverInstance.retrieveTextByXpath(Equipment_and_Tool_Category_PageObjects.POPRecordNumber_xpath()).split("#");
           setRecordId(retrieveMessage[1]);
           narrator.stepPassedWithScreenShot("Successfully 'Saved'. Record #" + getRecordId());    
         
        
        return true;
    }

}
