/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TestSuites;
import KeywordDrivenTestFramework.Entities.Enums;
import KeywordDrivenTestFramework.Reporting.Narrator;
import KeywordDrivenTestFramework.Testing.TestMarshall;
import KeywordDrivenTestFramework.Utilities.ApplicationConfig;
import java.io.FileNotFoundException;
import org.junit.Test;

/**
 *
 * @author smabe
 */
public class S5_2_Equipment_and_Tool_Category_TestSuite {
    static TestMarshall instance;

    public S5_2_Equipment_and_Tool_Category_TestSuite() {
        ApplicationConfig appConfig = new ApplicationConfig();
        TestMarshall.currentEnvironment = Enums.Environment.coreBeta;
    }

    
    //S5_2_Equipment_and_Tool_Category_QA01S5_2
    @Test    
    public void S5_2_Equipment_and_Tool_Category_QA01S5_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\S5_2_Equipment_and_Tool_Category_QA01S5_2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR1-Capture Equipment and Tool Category - Main Scenario
    @Test    
    public void FR1_Capture_Equipment_and_Tool_Category_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR1-Capture Equipment and Tool Category - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR2-Capture Asset Details - Main Scenario
    @Test
    public void FR2_Capture_Asset_Details_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR2-Capture Asset Details - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR2_Capture_Asset_Details_AlternateScenario_1() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR2-Capture Asset Details - Alternate Scenario 1.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR2_Capture_Asset_Details_AlternateScenario_2() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR2-Capture Asset Details - Alternate Scenario 2.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    @Test
    public void FR2_Capture_Asset_Details_OptionalScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR2-Capture Asset Details - Optional Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR3-Capture Asset Schedule - Main Scenario
    @Test
    public void FR3_Capture_Asset_Schedule_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR3 Capture Asset Schedule - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR4-Capture Equipment and Tools Asset Details Actions - Main Scenario
    @Test
    public void FR4_Capture_Equipment_and_Tools_Asset_Details_Actions_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR4-Capture Equipment and Tools Asset Details Actions - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR5-View Trained Personnel - Main Scenario
    @Test
    public void FR5_View_Trained_Personnel_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR5-View Trained Personnel - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }    
    
    //FR6-View Permitted Personnel - Main Scenario
    @Test
    public void FR6_View_Permitted_Personnel_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR6-View Permitted Personnel - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }
    
    //FR7-View Related Incidents - Main Scenario
    @Test
    public void FR7_View_Related_Incidents_MainScenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR7-View Related Incidents - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR8-View Related Risks - Main Scenario
    @Test
    public void FR8_View_Related_Risks_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR8-View Related Risks - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR9-View Inspections - Main Scenario
    @Test
    public void FR9_View_Inspections_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR9-View Inspections - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

    //FR10-View Hygiene Monitoring - Main Scenario
    @Test
    public void FR10_View_Hygiene_Monitoring_Main_Scenario() throws FileNotFoundException {
        Narrator.logDebug("Isometrix - V5.2 - Test Pack");
        instance = new TestMarshall("TestPacks\\Equipment and Tool Category v5.2\\FR10-View Hygiene Monitoring - Main Scenario.xlsx", Enums.BrowserType.Chrome);
        instance.runKeywordDrivenTests();
    }

}
